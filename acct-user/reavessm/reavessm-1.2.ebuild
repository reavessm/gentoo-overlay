# Copyright 2019-2024 Gentoo Authors
# Copyright 2024 Stephen M. Reaves
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="The all powerful user"
ACCT_USER_ID=1000
ACCT_USER_GROUPS=( "${PN}" "wheel" "usb" "plugdev" )
ACCT_USER_HOME=/home/"${PN}"
ACCT_USER_SHELL="/bin/bash"

IUSE="containers desktop extras games printers virt"

acct-user_add_deps

# rtkit Needed for pipewire
# x11-misc/ly might only be available in the supertux88 overlay
RDEPEND="
	acct-group/reavessm
	app-admin/stow
	app-admin/sudo
	app-editors/neovim
	app-misc/fastfetch
	app-misc/tmux
	dev-vcs/git
	dev-vcs/git-credential-manager
	media-fonts/nerdfonts[daddytimemono,firacode]
	media-fonts/noto-emoji
	net-fs/nfs-utils
	sys-apps/ripgrep
	sys-apps/tuned
	x11-misc/ly

	containers? (
		app-containers/crun
		app-containers/podman
	)
	virt? (
		app-emulation/libvirt[pcap,virt-network,numa,nfs,fuse,qemu]
		app-emulation/qemu[usbredir,spice,nfs,fuse]
		app-emulation/virt-manager[gui]
	)
	desktop? (
		app-admin/bitwarden-desktop-bin
		app-office/joplin-desktop
		app-office/libreoffice-bin
		dev-libs/light
		gui-apps/grim
		gui-apps/hypridle
		gui-apps/hyprlock
		gui-apps/hyprpaper
		gui-apps/slurp
		gui-apps/waybar
		gui-apps/wl-clipboard
		gui-apps/wl-mirror
		gui-apps/wofi
		gui-libs/xdg-desktop-portal-hyprland
		gui-wm/hyprland
		media-gfx/imv
		media-sound/pavucontrol
		media-video/obs-studio[browser]
		media-video/vlc[bluray,-vaapi]
		media-video/wireplumber
		net-misc/dhcpcd
		net-misc/networkmanager[wifi]
		sys-auth/rtkit
		www-client/firefox[hwaccel]
		x11-terms/kitty
		x11-themes/catppuccin-btop
		xfce-base/thunar
	)
	games? (
		games-util/gamemode
		games-util/steam-launcher
	)
	extras? (
		app-editors/vscode
		app-text/pandoc-bin
		dev-lang/elixir
		dev-lang/go
		dev-texlive/texlive-xetex
		dev-util/rustup
		media-gfx/imagemagick
		net-fs/sshfs
		net-libs/nodejs[npm]
		net-vpn/openconnect
		sci-visualization/gnuplot
		sys-apps/flatpak
		sys-apps/yarn
		sys-cluster/openmpi
		sys-process/btop
		virtual/rust
	)
	printers? (
		net-print/gutenprint
		net-print/hplip[minimal,-qt5]
	)
"

DEPEND="${RDEPEND}"
BDEPEND="${RDEPEND}"

pkg_setup() {
	use virt && ACCT_USER_GROUPS+=( kvm libvirt )
	use desktop && ACCT_USER_GROUPS+=( pipewire video )
}

pkg_postinst() {
	elog "Setting user to never expire"
	chage -E -1 "${PN}"

	ebegin "Setting up dot-files ..."

	mkdir -pv "${ACCT_USER_HOME}/Src/Personal"
	pushd "${ACCT_USER_HOME}/Src/Personal"

	if [[ -d "dot-files" ]]
	then
		elog "Pulling in existing directory ..."
		cd dot-files
		git -c safe.directory='*' pull || die
	else
		elog "Cloning dot-files ..."
		git -c safe.directory='*' clone "https://gitlab.com/${PN}/dot-files" || die
		cd dot-files
	fi

	su "${PN}" "./setup.sh" || die

	popd

	eend "Done!"

	ebegin "Fixing ownerships ..."

	chown "${PN}:${PN}" "/home/${PN}"

	eend "Done!"

	if use containers
	then
		elog "Make sure to check https://wiki.gentoo.org/wiki/Podman"
	fi

	if use virt
	then
		elog "Make sure to check https://wiki.gentoo.org/wiki/Libvirt and https://wiki.gentoo.org/wiki/Virt-manager and https://wiki.gentoo.org/wiki/QEMU"
	fi

	if use games
	then
		elog "Make sure to check https://wiki.gentoo.org/wiki/Steam"
	fi
}

pkg_prerm() {
	# Don't remove system user
	:
}
