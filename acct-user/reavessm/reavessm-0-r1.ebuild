# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="The all powerful user"
ACCT_USER_ID=1000
ACCT_USER_GROUPS=( "${PN}" "wheel" "video" "kvm" "usb" "pipewire" "plugdev" "libvirt" )
ACCT_USER_HOME=/home/"${PN}"
ACCT_USER_SHELL="/bin/bash"

acct-user_add_deps

# TODO: Move gnuplot to 'extras', move other stuff to other use flags
# rtkit Needed for pipewire
RDEPEND="
	acct-group/reavessm
	app-admin/bitwarden-desktop-bin
	app-admin/stow
	app-admin/sudo
	app-containers/podman
	app-editors/neovim
	app-emulation/libvirt[pcap,virt-network,numa,nfs,fuse,qemu]
	app-emulation/virt-manager[gui]
	app-emulation/qemu[usbredir,spice,nfs,fuse]
	app-misc/fastfetch
	app-misc/tmux
	app-office/joplin-desktop
	app-office/libreoffice-bin
	dev-lang/elixir
	dev-lang/go
	dev-libs/light
	dev-util/rustup
	dev-vcs/git
	dev-vcs/git-credential-manager
	games-util/gamemode
	games-util/steam-launcher
	gui-apps/grim
	gui-apps/hypridle
	gui-apps/hyprlock
	gui-apps/hyprpaper
	gui-apps/slurp
	gui-apps/waybar
	gui-apps/wl-clipboard
	gui-apps/wl-mirror
	gui-apps/wofi
	gui-libs/xdg-desktop-portal-hyprland
	gui-wm/hyprland
	media-fonts/nerdfonts[daddytimemono,firacode]
	media-fonts/noto-emoji
	media-gfx/imagemagick
	media-gfx/imv
	media-sound/pavucontrol
	media-video/obs-studio[browser]
	net-fs/nfs-utils
	net-misc/dhcpcd
	net-misc/networkmanager[wifi]
	sci-visualization/gnuplot
	sys-apps/flatpak
	sys-apps/ripgrep
	sys-apps/tuned
	sys-apps/yarn
	sys-auth/rtkit
	sys-process/btop
	virtual/rust
	www-client/firefox[hwaccel]
	x11-misc/ly
	x11-terms/kitty
	x11-themes/catppuccin-btop
	xfce-base/thunar
"

DEPEND="${RDEPEND}"
BDEPEND="${RDEPEND}"

pkg_postinst() {
	elog "Setting user to never expire"
	chage -E -1 "${PN}"

	ebegin "Setting up dot-files ..."

	mkdir -pv "${ACCT_USER_HOME}/Src/Personal"
	pushd "${ACCT_USER_HOME}/Src/Personal"

	if [[ -d "dot-files" ]]
	then
		elog "Pulling in existing directory ..."
		cd dot-files
		git -c safe.directory='*' pull || die
	else
		elog "Cloning dot-files ..."
		git -c safe.directory='*' clone "https://gitlab.com/${PN}/dot-files" || die
		cd dot-files
	fi

	su "${PN}" "./setup.sh" || die

	popd

	eend "Done!"

	ebegin "Fixing ownerships ..."

	chown -R "${PN}:${PN}" "/home/${PN}"

	eend "Done!"
}

pkg_prerm() {
	# Don't remove system user
	:
}
